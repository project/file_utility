CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Maintainers


INTRODUCTION
------------

File utility module for supporting file download feature 

CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Rahul Chauhan (rahulchauhan_ec3) - https://www.drupal.org/u/rahulchauhan_ec3
 * Dinesh Saini (dineshsaini) - https://www.drupal.org/u/dineshsaini


This project has been sponsored by:
 * Xeliumtech Solutions Pvt. Ltd.
   Founded in 2010, XeliumTech Solutions, a member of Nasscom, Started in 2010 
   with a vision to be a quality one-stop solutions company. Today, we are 
   proud to be a end to end trusted Software services partners for companies 
   globally across North America, Australia, UK,  Europe, Middle-East, 
   South-East Asia. Visit: https://www.drupal.org/xeliumtech-solutions-pvt-ltd 
   for more information.
