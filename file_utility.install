<?php

/**
 * @file
 * Defines the database tables used by this module.
 */

/**
 * Implements hook_schema().
 *
 * Defines the database tables used by this module.
 */
function file_utility_schema() {
  $schema['file_downbload_users'] = [
    'description' => 'The base table for entries of users who download any file.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'description' => 'The primary identifier for a user entry.',
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Name of the person.',
      ],
      'email' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Email Address of the person.',
      ],
      'file_path' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Path of the downloadable file.',
      ],
      'ip_address' => [
        'type' => 'varchar',
        'length' => 10,
        'not null' => TRUE,
        'default' => '',
        'description' => 'IP address of the person.',
      ],
      'count' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'No. of times the file is downloaded by same person.',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the entry was created.',
      ],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}
